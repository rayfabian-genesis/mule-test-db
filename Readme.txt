To run h2 server
java -cp h2-1.4.197.jar org.h2.tools.Server

Create a table
CREATE TABLE Customer (
    CUSTOMER_ID int NOT NULL AUTO_INCREMENT,
    NAME VARCHAR(35),
	ADDRESS VARCHAR(50),
    PRIMARY KEY (CUSTOMER_ID)
);
